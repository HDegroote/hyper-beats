import fs from 'fs/promises'
import Hyperbeats from './index.js'
import Corestore from 'corestore'
import ram from 'random-access-memory'

// note: only the metadata and the first 2 frames were uploaded
// so you won't actually be able to listen to the song here
const inName = './test/sample.flac'
const outName = './outio.flac'
const mimeType = 'flac'

console.log(`opening file at ${inName}`)
const file = await fs.open(inName)
const store = new Corestore(ram)
const core = store.get({ name: 'beats' })

const beats = new Hyperbeats(core)
await beats.write(file.createReadStream(), mimeType)
// The hypercore now contains the file, with 1 frame per block

const outFile = await fs.open(outName, 'w')
const outStream = beats.createReadStream()
const res = outStream.pipe(outFile.createWriteStream())
res.on('close', async () => {
  console.log('Get your flac at', outName)
  await beats.close()
})

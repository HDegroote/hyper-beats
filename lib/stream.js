const { ReadStream: HypercoreStream } = require('hypercore/lib/streams')

class HyperBeatsStream extends HypercoreStream {
  constructor (beats, startSec, endSec) {
    super(beats.core)

    this.beats = beats
    this.startSec = startSec
    this.endSec = endSec
  }

  async _openP () {
    await this.beats.ready()
    const { start, end } = await this.beats._secToBlocks(this.startSec, this.endSec)
    this.start = start
    this.end = end

    await super._openP()
  }
}

module.exports = HyperBeatsStream

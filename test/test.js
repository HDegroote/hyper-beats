const path = require('path')
const fs = require('fs/promises')
const { execFile } = require('node:child_process')
const util = require('node:util')
const cenc = require('compact-encoding')

const Hypercore = require('hypercore')
const Corestore = require('corestore')
const test = require('brittle')
const ram = require('random-access-memory')
const b4a = require('b4a')

const Hyperbeats = require('../index.js')
const { BeatsMetadataEnc } = require('../lib/encoding.js')

const FLAC_FILE_LOC = path.join(__dirname, 'sample.flac')
const MP3_FILE_LOC = path.join(__dirname, 'track.mp3')
const EXAMPLE_LOC = path.join(path.dirname(__dirname), 'example.mjs')

test('can write and stream', async t => {
  const beats = await getBeats(FLAC_FILE_LOC, 'flac')
  const core = beats.core

  t.is(core.length > 1, true)

  const fileBytes = await fs.readFile(FLAC_FILE_LOC)

  const res = await streamToArray(beats.createReadStream())
  t.alike(b4a.concat(res), fileBytes)
})

test('can get metadata', async t => {
  const beats = await getBeats(FLAC_FILE_LOC, 'flac')
  const metadata = await beats.getMetadata()

  t.is(metadata.year, 2023, 'correct year')
  t.is(metadata.artist, 'Jayvee Enaguas', 'correct artist')
  t.is(metadata.title, 'In the Flower Bay', 'correct title')
})

test('cover null if not present', async t => {
  const beats = await getBeats(FLAC_FILE_LOC, 'flac')
  const cover = await beats.getCover()

  // TODO: example which includes a cover
  t.is(cover, null, 'null cover if none available')
})

test('can get cover', async t => {
  const beats = await getBeats(MP3_FILE_LOC, 'mp3')
  const cover = await beats.getCover()
  t.is(cover.format, 'image/jpeg')
})

test('can stream section', async t => {
  const beats = await getBeats(FLAC_FILE_LOC, 'flac')

  // First frame ends at around 0.09
  // Frame size = 4058 bytes => 25 frames per block
  const outStreamF1 = beats.createReadStream({ start: 0, end: 0.05 })
  const outStreamBlocks1And2 = beats.createReadStream({ start: 0, end: 0.09 * 27 })
  const all = beats.createReadStream({ start: 0, end: 10000 })
  const nothing = beats.createReadStream({ start: 0, end: 0 })

  t.is((await streamToArray(outStreamF1)).length, 1)
  t.is((await streamToArray(outStreamBlocks1And2)).length, 2)
  t.is((await streamToArray(all)).length, 2)
  t.is((await streamToArray(nothing)).length, 0)
})

test('ensureValid returns true for valid beats', async t => {
  const beats = await getBeats(FLAC_FILE_LOC, 'flac')
  t.is(await beats.ensureValid(), true)
})

test('ensureValid throws for invalid beats', async t => {
  const core = new Hypercore(ram)
  await core.append('No beats here')
  const beats = new Hyperbeats(core)
  await beats.ready()
  await t.exception(
    async () => await beats.ensureValid(),
    /The core does not contain valid hyperbeats/
  )
})

test('Beats metadata encoding + versioning', t => {
  const metadata = {
    mimeType: 'audio/flac',
    framesPerBlock: 10000,
    bytesPerFrame: 800
  }

  const encoded = cenc.encode(BeatsMetadataEnc, metadata)

  const decoded = cenc.decode(BeatsMetadataEnc, encoded)
  const expected = {
    version: { major: 1, minor: 0 },
    ...metadata
  }
  t.alike(decoded, expected)

  t.alike(
    expected,
    cenc.decode(BeatsMetadataEnc, cenc.encode(BeatsMetadataEnc, expected)),
    'Encode(decode()) idempotent sanity check'
  )
})

test('cannot write to a non-empty hypercore', async t => {
  const core = new Hypercore(ram)
  await core.append('a block')

  const beats = new Hyperbeats(core)

  const file = await fs.open(FLAC_FILE_LOC)
  await t.exception(
    async () => await beats.write(file.createReadStream(), 'flac'),
    /Can only write hyperbeats to an empty core/,
    'expected error'
  )

  await file.close()
  await core.close()
})

test('example does not error', async t => {
  const res = await util.promisify(execFile)('node', [EXAMPLE_LOC])
  t.is(res.stderr, '')
})

test('get duration', async t => {
  const beats = await getBeats(MP3_FILE_LOC, 'mp3')
  const duration = await beats.getSecDuration()
  t.is(duration, 99)
})

async function getBeats (location, format) {
  const file = await fs.open(location)
  const store = new Corestore(ram)
  const core = store.get({ name: 'beats' })

  const beats = new Hyperbeats(core)
  await beats.ready()
  await beats.write(file.createReadStream(), format, { bytesPerBlock: 5000 })

  return beats
}

async function streamToArray (stream) {
  const res = []
  for await (const e of stream) res.push(e)

  return res
}

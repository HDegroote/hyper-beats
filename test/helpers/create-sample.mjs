import HyperBeats from './index.js'
import Corestore from 'corestore'
import fs from 'fs/promises'

const store = new Corestore('qt-store')
const core = store.get({ name: 'flacbeats' })

const beats = new HyperBeats(core)
await beats.ready()

const loc = 'orig.flac' // Change this
const fileHandle = await fs.open(loc)
await beats.write(fileHandle.createReadStream())

console.log('init beats written--length:', beats.core.length)

const otherCore = store.get({ name: 'otherCore' })
for await (const block of beats.core.createReadStream({ end: 3 })) {
  await otherCore.append(block)
}
console.log('other core written')

const outName = 'sample.flac'
const outFile = await fs.open(outName, 'w')

const outStream = await otherCore.createReadStream()
const res = outStream.pipe(outFile.createWriteStream())
res.on('close', async () => {
  console.log('Get your flac sample at', outName)
  await otherCore.close()
})

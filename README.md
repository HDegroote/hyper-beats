# Hyper Beats

Store and stream decentralised music.

Currently supports flac and MPEG (for example mp3).

## Install

npm i hyper-beats

## Usage

See [example.mjs](example.mjs)
